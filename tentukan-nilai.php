<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Function</title>
</head>
<body>
<?php
    function tentukan_nilai($number)
    {
       if ($number <= 100 && $number >= 80){echo "Sangat Baik";}
       if ($number < 80 && $number >= 70){echo "Baik";}
       if ($number < 70 && $number >= 60){echo "Cukup";}
       if ($number < 60){echo "Kurang";}
    }
    //TEST CASES
    echo tentukan_nilai(98). "<br>"; //Sangat Baik
    echo tentukan_nilai(76). "<br>"; //Baik
    echo tentukan_nilai(67). "<br>"; //Cukup
    echo tentukan_nilai(43). "<br>"; //Kurang
?>
</body>
</html>